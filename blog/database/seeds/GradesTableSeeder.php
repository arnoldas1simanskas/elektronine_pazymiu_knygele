<?php

use App\Grade;
use App\Lecture;
use App\Student;
use Faker\Factory;
use Illuminate\Database\Seeder;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();
        $faker = Faker\Factory::create();

        /*Pasirenkame kiek vienas studentas tures pazymiu*/
        for($i = 0; $i < 10; $i++){
            foreach ($students as $student){
                $grade = new Grade();
                $grade->student_id = $student->id;
                $grade->lecture_id = Lecture::inRandomOrder()->first()->id;
                $grade->grade = $faker->numberBetween(1, 10);
                $grade->save();
            }
        }

    }
}
