<?php

use App\Lecture;
use Faker\Factory;
use Illuminate\Database\Seeder;

class LecturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lectures = ['Math', 'Language', 'History', 'Sports', 'Informatics', 'Biology', 'Geographic\'s', 'Physics'];
        $faker = Faker\Factory::create();

        foreach ($lectures as $lecture){
            $lec = new Lecture();
            $lec->name = $lecture;
            $lec->description = $faker->text(500);

            $lec->save();
        }

    }
}
