<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Teacher';
        $user->email = 'test@teacher.lt';
        $user->password = bcrypt('secret');
        $user->role = 'teacher';
        $user->save();
    }
}
