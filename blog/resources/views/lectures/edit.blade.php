@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-md-center">
                <h1>{{__('Updating lecture')}}: {{$lecture->name}}</h1>
            </div>
            <div class="col-md-6">
                <form method="post" action="{{route('lectures.update', [$lecture->id])}}">
                    @csrf
                    @method('put')
                    <div>
                        <label>{{__('Name')}}:</label>
                        <input class="form-control" name="name" type="text" value="{{$lecture->name}}">
                    </div>
                    <div>
                        <label>{{__('Description')}}:</label>
                        <textarea class="form-control" name="description">{{$lecture->description}}</textarea>
                    </div>
                    <div>
                        <input class="col-md-6 mt-3 float-md-right btn-success form-control" type="submit" value="{{__('Update')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection