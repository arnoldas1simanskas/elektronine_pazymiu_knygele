@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <th>
                            {{__('ID')}}
                        </th>
                        <th>
                            {{__('Name')}}
                        </th>
                        <th>
                            {{__('Description')}}
                        </th>
                        <th>
                            {{__('Edit')}}
                        </th>
                        <th>
                            {{__('Delete')}}
                        </th>
                    </thead>
                    <tbody>
                    @foreach($lectures as $lecture)
                        <tr>
                            <td>
                                {{ $lecture->id }}
                            </td>
                            <td>
                                {{ $lecture->name }}
                            </td>
                            <td>
                                {!! $lecture->description !!}
                            </td>
                            <td>
                                <form method="get" action="{{ route('lectures.edit', [$lecture->id]) }}">
                                    <input class="btn btn-success" type="submit" value="Edit">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{{route('lectures.destroy', [$lecture->id])}}">
                                    @csrf
                                    @method('delete')
                                    <input class="btn btn-danger" type="submit" value="X">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection