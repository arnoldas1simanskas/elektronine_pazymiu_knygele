@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-md-center">
                <h1>{{__('Adding new lecture')}}</h1>
            </div>


            <div class="col-md-6">
                <form method="post" action="{{route('lectures.store')}}">
                    @csrf
                    <div>
                        <label>{{__('Name')}}:</label>
                        <input class="form-control" required name="name" type="text">
                    </div>
                    <div>
                        <label>{{__('Description')}}:</label>
                        <textarea class="form-control" required name="description"></textarea>
                    </div>

                    <div>
                        <input class="col-md-6 mt-3 float-md-right btn-success form-control" type="submit" value="{{__('Add')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection