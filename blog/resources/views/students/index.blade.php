@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>
                    {{__('Students')}} ({{$students->count()}})
                </h1>
                <table id="table_test" class="table">
                    <thead>
                        <th>
                            {{__('ID')}}
                        </th>
                        <th>
                            {{__('Surname')}}
                        </th>
                        <th>
                            {{__('Name')}}
                        </th>
                        <th>
                            {{__('Email')}}
                        </th>
                        <th>
                            {{__('Phone')}}
                        </th>
                        @if(isset(Auth::user()->role) && Auth::user()->role == 'teacher')
                        <th>
                            {{__('Edit')}}
                        </th>
                        <th>
                            {{__('Delete')}}
                        </th>
                        @endif
                    </thead>
                    <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td>
                                    <a href="{{route('students.show', [$student->id])}}">
                                        {{ $student->id }}
                                    </a>
                                </td>
                                <td>
                                    {{ $student->surname }}
                                </td>
                                <td>
                                    {{ $student->name }}
                                </td>
                                <td>
                                    {{ $student->email }}
                                </td>
                                <td>
                                    {{ $student->phone }}
                                </td>
                                @if(isset(Auth::user()->role) && Auth::user()->role == 'teacher')
                                <td>
                                    <form method="get" action="{{ route('students.edit', [$student->id]) }}">
                                        <input class="btn btn-success" type="submit" value="Edit">
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{{ route('students.destroy', [$student->id]) }}">
                                        @csrf
                                        @method('delete')
                                        <input class="btn btn-danger" type="submit" value="X">
                                    </form>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection