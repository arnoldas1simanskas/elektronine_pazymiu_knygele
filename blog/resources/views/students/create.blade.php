@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-md-center">
                <h1>{{__('Adding new student to your ranks')}}</h1>
            </div>
            <div class="col-md-6">
                <form method="post" action="{{route('students.store')}}">
                    @csrf
                    <div>
                        <label>{{__('Name')}}:</label>
                        <input class="form-control @error('name') is-invalid @enderror" required name="name" type="text" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <label>{{__('Surname')}}:</label>
                        <input class="form-control @error('surname') is-invalid @enderror" required name="surname" type="text" value="{{ old('surname') }}">
                        @error('surname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <label>{{__('Email')}}:</label>
                        <input class="form-control @error('email') is-invalid @enderror" required name="email" type="email" value="{{ old('email') }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <label>{{__('Phone')}}:</label>
                        <input class="form-control @error('phone') is-invalid @enderror" required name="phone" type="text" value="{{ old('phone') }}">
                        @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div>
                        <input class="col-md-6 mt-3 float-md-right btn-success form-control" type="submit" value="{{__('Add')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
