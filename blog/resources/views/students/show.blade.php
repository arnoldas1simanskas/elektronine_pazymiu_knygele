@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>{{__('Student')}} {{ $student->surname }} {{ $student->name }} {{__('profile')}}</h3>
                <ul>
                    <li>
                        {{__('Name')}}: {{$student->name}}
                    </li>
                    <li>
                        {{__('Surname')}}: {{$student->surname}}
                    </li>
                    <li>
                        {{__('Email')}}: {{$student->email}}
                    </li>
                    <li>
                        {{__('Phone')}}: {{$student->phone}}
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h4>{{__('Grades')}}({{ $student->grades()->count() }}):</h4>
                <div>
                    <table class="table">
                        <thead>
                            <th>
                                {{__('Lecture')}}
                            </th>
                            <th>
                                {{__('Grade')}}
                            </th>
                            <th>
                                {{__('When')}}
                            </th>
                        </thead>
                        <tbody>

                            @foreach($student->grades() as $grade)
                                <tr>
                                @foreach($grade->lecture() as $lecture)
                                    <td>
                                        {{$lecture->name}}
                                    </td>
                                @endforeach
                                    <td>
                                        {{$grade->grade}}
                                    </td>
                                    <td>
                                        {{$grade->created_at}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection