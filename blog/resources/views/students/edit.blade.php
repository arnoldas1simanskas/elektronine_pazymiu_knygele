@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-md-center">
                <h1>{{__('Editing student')}}: {{ $student->surname }} {{ $student->name }}</h1>
            </div>
            <div class="col-md-6">
                <form method="post" action="{{route('students.update', [$student->id])}}">
                    @csrf
                    @method('put')
                    <div>
                        <label>{{__('Name')}}:</label>
                        <input class="form-control" name="name" type="text" value="{{$student->name}}">
                    </div>
                    <div>
                        <label>{{__('Surname')}}:</label>
                        <input class="form-control" name="surname" type="text" value="{{$student->surname}}">
                    </div>
                    <div>
                        <label>{{__('Email')}}:</label>
                        <input class="form-control" name="email" type="email" value="{{$student->email}}">
                    </div>
                    <div>
                        <label>{{__('Phone')}}:</label>
                        <input class="form-control" name="phone" type="text" value="{{$student->phone}}">
                    </div>
                    <div>
                        <input class="col-md-6 mt-3 float-md-right btn-success form-control" type="submit" value="{{__('Update')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
