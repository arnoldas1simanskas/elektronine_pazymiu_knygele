@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Wellcome')}}, {{ Auth::user()->name }} {{__('to your Dashboard')}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-md-5 col-sm-5 col-lg-5 mt-2">
                            <a class="btn btn-success form-control" href="{{ route('students.index') }}">{{__('All students')}}</a>
                        </div>
                        @if(Auth::user()->role == 'teacher')
                        <div class="col-md-5 col-sm-5 col-lg-5 mt-2">
                            <a class="btn btn-success form-control" href="{{ route('students.create') }}">{{__('Add new student')}}</a>
                        </div>
                        <div class="col-md-5 col-sm-5 col-lg-5 mt-2">
                            <a class="btn btn-success form-control" href="{{ route('lectures.index') }}">{{__('All lectures')}}</a>
                        </div>
                        <div class="col-md-5 col-sm-5 col-lg-5 mt-2">
                            <a class="btn btn-success form-control" href="{{ route('lectures.create') }}">{{__('Add new lecture')}}</a>
                        </div>
                        <div class="col-md-5 col-sm-5 col-lg-5 mt-2">
                            <a class="btn btn-success form-control" href="{{ route('grades.create') }}">{{__('Give student a grade')}}</a>
                        </div>
                        @endif
                    </div>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
