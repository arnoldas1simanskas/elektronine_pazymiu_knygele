@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-md-center">
                <h1>{{__('Giving grade to a student')}}</h1>
            </div>
            <div class="col-md-6">
                <form method="post" action="{{route('grades.store')}}">
                    @csrf
                    <label>{{__('Lecture')}}:</label>
                    <select class="form-control" name="lecture_id" required>
                        <option value="">{{__('Choose lecture')}}</option>
                        @foreach($lectures as $lecture)
                            @if($lecture->id == old('lecture_id'))
                                <option selected value="{{ $lecture->id }}">{{ $lecture->name }}</option>
                            @else
                                <option value="{{ $lecture->id }}">{{ $lecture->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    <label>{{__('Student')}}:</label>
                    <select class="form-control" name="student_id" required>
                        <option value="">{{__('Choose student')}}</option>
                        @foreach($students as $student)
                            @if($student->id == old('student_id'))
                                <option selected value="{{ $student->id }}">
                                    {{ $student->name }}
                                    {{ $student->surname }}
                                </option>
                            @else
                                <option value="{{ $student->id }}">
                                    {{ $student->name }}
                                    {{ $student->surname }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                    <div>
                        <label>{{__('Grade')}}:</label>
                        <input class="form-control" required name="grade" type="number">
                    </div>
                    <div>
                        <input class="col-md-6 mt-3 float-md-right btn-success form-control" type="submit" value="{{__('Add')}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection