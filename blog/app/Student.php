<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $table = 'students';

    protected $guarded = ['id'];

    public function grades(){
        return $this->hasMany('App\Grade', 'student_id', 'id')->get();
    }
    public function allGrades(){
        return $this->belongsToMany(
            'App\Grade',
            'grades',
            'lectures_id',
            'students_id'
        )->get();
    }
}
