<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';

    public function allGrades(){
        return $this->belongsToMany(
            'App\Student',
            'lectures',
            'lectures_id',
            'students_id'
            );
    }
    public function lecture(){
        return $this->hasOne('App\Lecture', 'id', 'lecture_id')->get();
    }

    public function student(){
        return $this->hasOne('App\Student', 'id', 'student_id')->get();
    }
}
