<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => 'A :attribute is required',
            'min'  => ':attribute is too short, min value :min symbols',
            'max'  => ':attribute is too long, max value :min symbols',
            'unique' => 'this :attribute already exists'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20',
            'surname' => 'required|min:2|max:20',
            //unique pirma table from db ir po to column
            'email' => 'required|email|unique:students,email',
            'phone' => 'required|numeric',
        ];
    }
}
