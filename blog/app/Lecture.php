<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $table = 'lectures';

    protected $guarded =['id'];

    public function lectures(){
        return $this->hasMany('App\Grade', 'lecture_id', 'id');
    }
}
